<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/mots_obligatoires.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mot_manquant' => 'Pour publier, ajouter un mot-clé du groupe suivant :',
	'mots_manquants' => 'Pour publier, ajouter un mot-clé des groupes suivants&nbsp :'
);
