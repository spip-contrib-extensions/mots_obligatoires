<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/mots_obligatoires?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mot_manquant' => 'Para publicar, incluir uma palavra-chave do grupo a seguir:',
	'mots_manquants' => 'Para publicar, incluir uma palavra-chave dos grupos a seguir:'
);
