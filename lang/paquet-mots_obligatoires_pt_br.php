<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mots_obligatoires?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mots_obligatoires_description' => 'Este plugin impede a publicação de objetos (matérias, documentos etc.) que não tenham vinculada nenhuma palavra-chave de um grupo definido como importante.',
	'mots_obligatoires_slogan' => 'Só publicar os objetos corretamente etiquetados'
);
